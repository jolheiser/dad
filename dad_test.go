package dad

import (
	"context"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

var (
	server *httptest.Server
	client *Client

	searchResults = []byte(`{"current_page":1,"limit":20,"next_page":2,"previous_page":1,"results":[{"id":"M7wPC5wPKBd","joke":"Did you hear the one about the guy with the broken hearing aid? Neither did he."},{"id":"MRZ0LJtHQCd","joke":"What do you call a fly without wings? A walk."},{"id":"usrcaMuszd","joke":"What's the worst thing about ancient history class? The teachers tend to Babylon."}],"search_term":"","status":200,"total_jokes":307,"total_pages":15}`)
	termResults   = []byte(`{"current_page":1,"limit":20,"next_page":1,"previous_page":1,"results":[{"id":"GlGBIY0wAAd","joke":"How much does a hipster weigh? An instagram."},{"id":"xc21Lmbxcib","joke":"How did the hipster burn the roof of his mouth? He ate the pizza before it was cool."}],"search_term":"hipster","status":200,"total_jokes":2,"total_pages":1}`)
	jokeResult    = []byte(`{"id":"R7UfaahVfFd","joke":"My dog used to chase people on a bike a lot. It got so bad I had to take his bike away.","status":200}`)
	joke          = Joke{
		ID:     "R7UfaahVfFd",
		Joke:   "My dog used to chase people on a bike a lot. It got so bad I had to take his bike away.",
		Status: 200,
	}
	search = &Jokes{
		CurrentPage:  1,
		Limit:        20,
		NextPage:     2,
		PreviousPage: 1,
		Jokes:        []*Joke{{}, {}, {}},
		Status:       200,
		TotalJokes:   307,
		TotalPages:   15,
	}
	term = &Jokes{
		CurrentPage:  1,
		Limit:        20,
		NextPage:     1,
		PreviousPage: 1,
		Jokes:        []*Joke{{}, {}},
		SearchTerm:   "hipster",
		Status:       200,
		TotalJokes:   2,
		TotalPages:   1,
	}
)

func TestMain(m *testing.M) {
	handler := func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/" {
			_, _ = w.Write(jokeResult)
			return
		}
		if r.URL.Path == "/j/R7UfaahVfFd" {
			_, _ = w.Write(jokeResult)
			return
		}
		if r.URL.Path == "/search" {
			if r.URL.Query().Get("term") == "hipster" {
				_, _ = w.Write(termResults)
				return
			}
			_, _ = w.Write(searchResults)
			return
		}
		w.WriteHeader(http.StatusNotFound)
	}

	server = httptest.NewServer(http.HandlerFunc(handler))
	baseURL = server.URL + "/"
	client = New()

	os.Exit(m.Run())
}

func TestJoke(t *testing.T) {
	t.Parallel()

	// Random Joke
	j, err := client.Random(context.Background())
	if err != nil {
		t.Logf("could not get random joke: %v", err)
		t.FailNow()
	}

	if *j != joke {
		t.Log("joke did not match test data")
		t.FailNow()
	}

	// Get the random joke and make sure it's the same
	j, err = client.Joke(context.Background(), j.ID)
	if err != nil {
		t.Logf("could not get specific joke: %v", err)
		t.FailNow()
	}

	if *j != joke {
		t.Log("joke did not match test data")
		t.FailNow()
	}
}

func TestSearch(t *testing.T) {
	t.Parallel()

	results, err := client.Search(context.Background(), SearchOptions{})
	if err != nil {
		t.Logf("could not search jokes: %v\n", err)
		t.FailNow()
	}

	if !compare(results, search) {
		t.Log("jokes did not match test data")
		t.FailNow()
	}
}

func TestSearchTerm(t *testing.T) {
	t.Parallel()

	results, err := client.Search(context.Background(), SearchOptions{
		Term: "hipster",
	})
	if err != nil {
		t.Logf("could not search jokes with term: %v\n", err)
		t.FailNow()
	}

	if !compare(results, term) {
		t.Log("jokes did not match test data")
		t.FailNow()
	}
}

func compare(j1, j2 *Jokes) bool {
	return j1.CurrentPage == j2.CurrentPage &&
		j1.Limit == j2.Limit &&
		j1.NextPage == j2.NextPage &&
		j1.PreviousPage == j2.PreviousPage &&
		len(j1.Jokes) == len(j2.Jokes) &&
		j1.SearchTerm == j2.SearchTerm &&
		j1.Status == j2.Status &&
		j1.TotalJokes == j2.TotalJokes &&
		j1.TotalPages == j2.TotalPages
}
