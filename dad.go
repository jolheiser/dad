package dad

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
)

var baseURL = "https://icanhazdadjoke.com/"

// Joke is a joke from ICanHazDadJoke
type Joke struct {
	ID     string `json:"id"`
	Joke   string `json:"joke"`
	Status int    `json:"status"`
}

// Jokes is a result of a Search
type Jokes struct {
	CurrentPage  int     `json:"current_page"`
	Limit        int     `json:"limit"`
	NextPage     int     `json:"next_page"`
	PreviousPage int     `json:"previous_page"`
	Jokes        []*Joke `json:"results"`
	SearchTerm   string  `json:"search_term"`
	Status       int     `json:"status"`
	TotalJokes   int     `json:"total_jokes"`
	TotalPages   int     `json:"total_pages"`
}

// Image returns a URL for an image of this Joke
func (j *Joke) Image() string {
	return fmt.Sprintf("%sj/%s.png", baseURL, j.ID)
}

// Random returns a random Joke
func (c *Client) Random(ctx context.Context) (*Joke, error) {
	return c.Joke(ctx, "")
}

// Joke returns a specific Joke
func (c *Client) Joke(ctx context.Context, id string) (*Joke, error) {
	u := baseURL
	if id != "" {
		u += "j/" + id
	}

	req, err := newRequest(ctx, http.MethodGet, u, nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.http.Do(req)
	if err != nil {
		return nil, err
	}

	var joke Joke
	if err := json.NewDecoder(resp.Body).Decode(&joke); err != nil {
		return nil, err
	}

	return &joke, resp.Body.Close()
}

// SearchOptions is options for a search
type SearchOptions struct {
	Page  int
	Limit int
	Term  string
}

// Values turns SearchOptions into url.Values
func (so SearchOptions) Values() url.Values {
	var val = make(url.Values)

	if so.Page != 0 {
		val.Set("page", strconv.Itoa(so.Page))
		if so.Page < 1 {
			val.Set("page", "1")
		}
	}

	if so.Limit != 0 {
		val.Set("limit", strconv.Itoa(so.Limit))
		if so.Limit < 1 {
			val.Set("limit", "1")
		} else if so.Limit > 30 {
			val.Set("limit", "30")
		}
	}

	if so.Term != "" {
		val.Set("term", so.Term)
	}

	return val
}

// Search returns a list of Joke
func (c *Client) Search(ctx context.Context, so SearchOptions) (*Jokes, error) {
	u := fmt.Sprintf("%ssearch?%s", baseURL, so.Values().Encode())

	req, err := newRequest(ctx, http.MethodGet, u, nil)
	if err != nil {
		return nil, err
	}

	resp, err := c.http.Do(req)
	if err != nil {
		return nil, err
	}

	var results Jokes
	if err := json.NewDecoder(resp.Body).Decode(&results); err != nil {
		return nil, err
	}

	// Set status of jokes for posterity
	for _, j := range results.Jokes {
		j.Status = results.Status
	}

	return &results, resp.Body.Close()
}
