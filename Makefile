GO ?= go

.PHONY: build
build:
	$(GO) build ./cmd/dad

.PHONY: vet
vet:
	$(GO) vet ./...

.PHONY: fmt
fmt:
	$(GO) fmt ./...

.PHONY: test
test:
	$(GO) test -race ./...
