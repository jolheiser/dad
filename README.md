# dad

An API client for [ICanHazDadJoke](https://icanhazdadjoke.com).

My only regret is not implementing their [GraphQL API](https://icanhazdadjoke.com/api#graphql).

For more information on how this library *should* work, check out their [API Docs](https://icanhazdadjoke.com/api).

## License

[MIT](LICENSE)