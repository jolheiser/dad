package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"

	"go.jolheiser.com/dad"
)

var (
	imageFlag bool
	outFlag   string
)

func main() {
	flag.BoolVar(&imageFlag, "image", false, "get the image response for a joke")
	flag.StringVar(&outFlag, "out", "", "where to write the response (default: stdout)")
	flag.Parse()

	var jokeID string
	if flag.NArg() > 0 {
		jokeID = flag.Arg(0)
	}

	out := os.Stdout
	if outFlag != "" {
		fi, err := os.Create(outFlag)
		if err != nil {
			fmt.Printf("Could not create file %s: %v\n", outFlag, err)
			return
		}
		defer fi.Close()
		out = fi
	}

	if err := run(jokeID, out); err != nil {
		fmt.Println(err)
	}
}

func run(jokeID string, out io.WriteCloser) error {

	client := dad.New()

	var joke *dad.Joke
	var err error
	if jokeID != "" {
		joke, err = client.Joke(context.Background(), jokeID)
	} else {
		joke, err = client.Random(context.Background())
	}
	if err != nil {
		return err
	}

	if imageFlag {
		return image(joke.Image(), out)
	}

	if _, err := fmt.Fprintln(out, joke.Joke); err != nil {
		return err
	}
	return nil
}

func image(img string, out io.WriteCloser) error {
	req, err := http.NewRequest(http.MethodGet, img, nil)
	if err != nil {
		return err
	}
	req.Header.Set("User-Agent", "go.jolheiser.com/dad/cmd/dad")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if _, err := io.Copy(out, resp.Body); err != nil {
		return err
	}
	return nil
}
