package dad

import (
	"context"
	"io"
	"net/http"
)

// Client is an ICanHazDadJoke client
type Client struct {
	http *http.Client
}

// New returns a new ICanHazDadJoke Client
func New(opts ...ClientOption) *Client {
	c := &Client{
		http: http.DefaultClient,
	}
	for _, opt := range opts {
		opt(c)
	}
	return c
}

// ClientOption is options for a Client
type ClientOption func(*Client)

// WithHTTP sets the http.Client for the XKCD Client
func WithHTTP(client *http.Client) ClientOption {
	return func(c *Client) {
		c.http = client
	}
}

func newRequest(ctx context.Context, method string, url string, body io.Reader) (*http.Request, error) {
	req, err := http.NewRequestWithContext(ctx, method, url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Accept", "application/json")
	req.Header.Set("User-Agent", "go.jolheiser.com/dad")
	return req, nil
}
